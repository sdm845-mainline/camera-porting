# camera porting notes

Random notes and docs on porting camera sensors to mainline for SDM845 devices.

## Recommended Reading

 * https://www.96boards.org/documentation/consumer/dragonboard/dragonboard410c/guides/camera-module.md.html

## Potentially useful resources

* Try to confirm that the ISP at least works with YAVTA before attempting sensor bringup again: https://git.ideasonboard.org/yavta.git

## Downstream CAMSS Debugging

Implemented custom CAMSS debugging to understand the downstrean driver better. The kernel code and debugging logs can be found below. The kernel is based on Lineage 20. 

**Poco F1**

<ins>Kernel</ins> 

https://gitlab.com/sdm845-mainline/android_kernel_xiaomi_sdm845_downstream/-/commits/camss-debug

<ins>CAMSS Logs - Single photo taken using IMX363 back camera</ins>

check `pocof1_camss_csiphy_csid_vfetop_vfe_bus.log` file in `downstream_camss_debugging_logs` folder in this repo.

**Oneplus 6**

<ins>Kernel</ins> 

https://github.com/joelselvaraj/android_kernel_oneplus_sdm845/commits/camss_debug

<ins>CAMSS Logs - Single photo taken using IMX519 back camera</ins>

check `op6_camss_csiphy_csid_vfetop_vfebus.log` file in `downstream_camss_debugging_logs` folder in this repo.
