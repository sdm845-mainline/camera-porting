#!/bin/sh

set -x
set -e

# $1: ep
set_fmt() {
    EP="$1"
    sudo media-ctl --set-v4l2 "${EP}:0[fmt:SRGGB10_1X10/1920x1080 field:none]"
}

# $1: source
# $2: sink
set_link() {
    SOURCE="$1"
    SINK="$2"
    sudo media-ctl --links "${SOURCE}:1->${SINK}:0[1]"
}

set_fmt 1
set_fmt 13
set_fmt 22

set_link 1 13
set_link 13 22
