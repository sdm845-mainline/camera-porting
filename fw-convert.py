#!/bin/python3

import struct

fw_txt = open("sensor-fw.txt", "r").readlines()
fw = [int(x[:-1], 16) for x in fw_txt]
packed = struct.pack("2580B", *fw)

fw_bin = open("sensor_fw.bin", "wb")
fw_bin.write(packed)
fw_bin.close()
